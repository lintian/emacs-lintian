;;; lintian.el --- Examine Lintian packaging hints in Emacs

;; Copyright © 2021 Felix Lechner

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Author: Felix Lechner <felix.lechner@lease-up.com>
;; Version: 0.1
;; Package-Requires: ((shr))
;; Keywords: packaging, lintian, debian
;; URL: https://lintian.debian.org

;;; Commentary:

;; This package provides a command to run Lintian in Emacs. The purpose is
;; to provide extra functionality when examining the resulting packaging hints

;; Thanks to bpalmer on Libera:#emacs for help with this module

(require 'shr)

(defcustom lintian-command "lintian" "*The path to the lintian executable")

;;;###autoload
(defun lintian-run-file
    (file) "Runs the lintian executable on the specified FILE."
  (interactive "fFile: ")
  (let
      ((newbuf
        (get-buffer-create "*packaging-hints*")))
    (switch-to-buffer newbuf)
    (erase-buffer)
    (call-process lintian-command nil newbuf nil "--exp-output" "format=html" file)
    (shr-render-region
     (point-min)
     (point-max))
    (goto-char (point-min))))

(provide 'lintian)

;;; lintian.el ends here
